---
AWSTemplateFormatVersion: 2010-09-09
Parameters:
  LatestAmiId:
    Type: 'AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>'
    Default: '/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2'
  KeyName:
    Type: 'AWS::EC2::KeyPair::KeyName'
    Default: 'MyFirstKey'
  InstanceType:
    Description: 'Free InstanceType'
    Type: 'String'
    Default: 't2.micro'
    AllowedValues:
      - 't2.micro'
  DeletionProtection:
    Description: 'Free InstanceType'
    Type: 'String'
    Default: 'false'
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 10.1.0.0/16
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
      - Key: Name
        Value:  !Sub ${AWS::StackName}-VPC
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    DependsOn: VPC
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway
  PublicSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      CidrBlock: 10.1.10.0/24
      AvailabilityZone: !Select [ 0, !GetAZs ]       
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-A
  PublicSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      CidrBlock: 10.1.20.0/24
      AvailabilityZone: !Select [ 1, !GetAZs ]
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-B
  PublicSubnetC:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      CidrBlock: 10.1.30.0/24
      AvailabilityZone: !Select [ 2, !GetAZs ] 
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-C
  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-RouteTable
  PublicRoute1:
    Type: AWS::EC2::Route
    DependsOn: AttachGateway
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway 
  PublicSubnetARouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetA
      RouteTableId: !Ref PublicRouteTable
  PublicSubnetBRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetB
      RouteTableId: !Ref PublicRouteTable
  PublicSubnetCRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetC
      RouteTableId: !Ref PublicRouteTable
  EC2SecurityGroupWEB:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref VPC
      GroupName: !Sub ${AWS::StackName}-NSG-WEB
      GroupDescription: WEB
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '3306'
          ToPort: '3306'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '2049'
          ToPort: '2049'
          CidrIp: 0.0.0.0/0         
      Tags:
        - Key: Name
          Value: SG-WEB
  EC2SecurityGroupALB:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref VPC
      GroupName: !Sub ${AWS::StackName}-NSG-ALB
      GroupDescription: ALB
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: 0.0.0.0/0         
      Tags:
        - Key: Name
          Value: SG-ALB
  InboundRule:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !GetAtt EC2SecurityGroupWEB.GroupId
      IpProtocol: tcp
      FromPort: '80'
      ToPort: '80'
      SourceSecurityGroupId: !GetAtt EC2SecurityGroupALB.GroupId       
  LB01:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        - Key: deletion_protection.enabled
          Value: !Ref DeletionProtection
      IpAddressType: ipv4
      Name: !Sub ${AWS::StackName}-LB01
      Scheme: internet-facing
      SecurityGroups: 
        - !Ref EC2SecurityGroupALB
      Subnets: 
        - !Ref PublicSubnetA
        - !Ref PublicSubnetB
        - !Ref PublicSubnetC
      Tags: 
        - Key: Name
          Value: !Sub ${AWS::StackName}-LB
      Type: application
  Listener:
    Type: 'AWS::ElasticLoadBalancingV2::Listener'
    Properties:
      DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref TargetGroup01
      LoadBalancerArn: !Ref LB01
      Port: '80'
      Protocol: HTTP
  TargetGroup01:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      Name: !Sub ${AWS::StackName}-TargetGroup
      Port: '80'
      Protocol: HTTP
      VpcId: !Ref VPC
  ASG01:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      AutoScalingGroupName: !Sub ${AWS::StackName}-ASG01
      MinSize: "1"
      MaxSize: "3"
      DesiredCapacity: "2"
      HealthCheckGracePeriod: 300
      LaunchTemplate:
        LaunchTemplateId: !Ref Template01
        Version: !GetAtt Template01.LatestVersionNumber
      VPCZoneIdentifier:
        - !Ref PublicSubnetA
        - !Ref PublicSubnetB
        - !Ref PublicSubnetC
      Tags:
        - Key: Name
          PropagateAtLaunch: true
          Value: !Sub ${AWS::StackName}-WordPressServer
      TargetGroupARNs:
        - !Ref TargetGroup01
  Template01:
    Type: AWS::EC2::LaunchTemplate
    DependsOn: 
    - LB01
    Properties:
      LaunchTemplateData:
        IamInstanceProfile: 
          Arn: arn:aws:iam::252893954498:instance-profile/CodeDeplyAgentRole
        DisableApiTermination: !Ref DeletionProtection
        InstanceType: !Ref InstanceType
        KeyName: !Ref KeyName
        ImageId: !Ref LatestAmiId
        SecurityGroupIds:
        - !GetAtt EC2SecurityGroupWEB.GroupId
        UserData:
          Fn::Base64:
            !Sub |
              #!/bin/bash
              yum -y update
              yum install -y ruby
              yum install -y aws-cli
              aws s3 cp s3://aws-codedeploy-us-east-2/latest/install . --region eu-west-1
              chmod +x ./install
              ./install auto
      LaunchTemplateName: !Sub ${AWS::StackName}-Template01
  Repository:
    Type: AWS::CodeCommit::Repository
    Properties:
      Code:
          S3:  
            Bucket: aws-codedeploy-eu-west-1
            Key: samples/latest/SampleApp_Linux.zip
      RepositoryDescription: "An app that will be automatically deployed using codepipeline"
      RepositoryName: !Sub ${AWS::StackName}-DemoPipelineApp
      Tags: 
        - Key: Name
          Value: !Sub ${AWS::StackName}-Repository
  Application:
    Type: AWS::CodeDeploy::Application
    Properties: 
      ApplicationName: !Sub ${AWS::StackName}-Application
      ComputePlatform: Server
  DeploymentConfig:
    Type: AWS::CodeDeploy::DeploymentConfig
    Properties:
      MinimumHealthyHosts:
        Type: FLEET_PERCENT
        Value: '25'
  SNSTopic:
    Type: AWS::SNS::Topic
    Properties: {}
  DeploymentGroup:
    Type: AWS::CodeDeploy::DeploymentGroup
    Properties:
      ApplicationName: !Ref Application
      DeploymentConfigName: !Ref DeploymentConfig
      DeploymentGroupName: !Sub ${AWS::StackName}-DeploymentGroup
      AutoScalingGroups:
        - !Ref ASG01 
      LoadBalancerInfo:
        ElbInfoList:
          - Name: !Ref LB01
      DeploymentStyle:
        DeploymentOption: WITH_TRAFFIC_CONTROL
        DeploymentType: IN_PLACE
      ServiceRoleArn: arn:aws:iam::252893954498:role/CodeDeployServiceRole