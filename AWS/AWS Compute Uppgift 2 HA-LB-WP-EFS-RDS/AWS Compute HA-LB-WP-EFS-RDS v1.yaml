---
AWSTemplateFormatVersion: 2010-09-09
Parameters:
  LatestAmiId:
    Type: 'AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>'
    Default: '/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2'
  KeyName:
    Type: 'AWS::EC2::KeyPair::KeyName'
    Default: 'MyFirstKey'
  InstanceType:
    Description: 'Free InstanceType'
    Type: 'String'
    Default: 't2.micro'
    AllowedValues:
      - 't2.micro'
  DeletionProtection:
    Description: 'Free InstanceType'
    Type: 'String'
    Default: 'false'
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 10.1.0.0/16
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
      - Key: Name
        Value:  !Sub ${AWS::StackName}-VPC
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    DependsOn: VPC
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway
  PublicSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      CidrBlock: 10.1.10.0/24
      AvailabilityZone: !Select [ 0, !GetAZs ]       
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-A
  PublicSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      CidrBlock: 10.1.20.0/24
      AvailabilityZone: !Select [ 1, !GetAZs ]
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-B
  PublicSubnetC:
    Type: AWS::EC2::Subnet
    Properties:
      MapPublicIpOnLaunch: true
      VpcId: !Ref VPC
      CidrBlock: 10.1.30.0/24
      AvailabilityZone: !Select [ 2, !GetAZs ] 
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-C
  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-Public-RouteTable
  PublicRoute1:
    Type: AWS::EC2::Route
    DependsOn: AttachGateway
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway 
  PublicSubnetARouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetA
      RouteTableId: !Ref PublicRouteTable
  PublicSubnetBRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetB
      RouteTableId: !Ref PublicRouteTable
  PublicSubnetCRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId: !Ref PublicSubnetC
      RouteTableId: !Ref PublicRouteTable
  MountTargetSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref VPC      
      GroupName: !Sub ${AWS::StackName}-NSG-MTS
      GroupDescription: WEB
      SecurityGroupIngress:
        - IpProtocol: tcp          
          FromPort: '2049'
          ToPort: '2049'
          CidrIp: 0.0.0.0/0 
      Tags:
        - Key: Name
          Value: SG-MTS
  EC2SecurityGroupWEB:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref VPC
      GroupName: !Sub ${AWS::StackName}-NSG-WEB
      GroupDescription: WEB
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '3306'
          ToPort: '3306'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '2049'
          ToPort: '2049'
          CidrIp: 0.0.0.0/0         
      Tags:
        - Key: Name
          Value: SG-WEB
  EC2SecurityGroupALB:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref VPC
      GroupName: !Sub ${AWS::StackName}-NSG-ALB
      GroupDescription: ALB
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: 0.0.0.0/0         
      Tags:
        - Key: Name
          Value: SG-ALB
  InboundRule:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !GetAtt EC2SecurityGroupWEB.GroupId
      IpProtocol: tcp
      FromPort: '80'
      ToPort: '80'
      SourceSecurityGroupId: !GetAtt EC2SecurityGroupALB.GroupId
  DatabaseSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      VpcId: !Ref VPC
      GroupName: !Sub ${AWS::StackName}-NSG-RDS
      GroupDescription: DB
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '3306'
          ToPort: '3306'
          CidrIp: 0.0.0.0/0
      Tags:
        - Key: Name
          Value: SG-RDS          
  LB01:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        - Key: deletion_protection.enabled
          Value: !Ref DeletionProtection
      IpAddressType: ipv4
      Name: !Sub ${AWS::StackName}-LB01
      Scheme: internet-facing
      SecurityGroups: 
        - !Ref EC2SecurityGroupALB
      Subnets: 
        - !Ref PublicSubnetA
        - !Ref PublicSubnetB
        - !Ref PublicSubnetC
      Tags: 
        - Key: Name
          Value: !Sub ${AWS::StackName}-LB
      Type: application
  Listener:
    Type: 'AWS::ElasticLoadBalancingV2::Listener'
    Properties:
      DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref TargetGroup01
      LoadBalancerArn: !Ref LB01
      Port: '80'
      Protocol: HTTP
  TargetGroup01:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      Name: !Sub ${AWS::StackName}-TargetGroup
      Port: '80'
      Protocol: HTTP
      VpcId: !Ref VPC
  ASG01:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      AutoScalingGroupName: !Sub ${AWS::StackName}-ASG01
      MinSize: "1"
      MaxSize: "3"
      DesiredCapacity: "2"
      HealthCheckGracePeriod: 300
      LaunchTemplate:
        LaunchTemplateId: !Ref Template01
        Version: !GetAtt Template01.LatestVersionNumber
      VPCZoneIdentifier:
        - !Ref PublicSubnetA
        - !Ref PublicSubnetB
        - !Ref PublicSubnetC
      Tags:
        - Key: Name
          PropagateAtLaunch: true
          Value: !Sub ${AWS::StackName}-WordPressServer
      TargetGroupARNs:
        - !Ref TargetGroup01
  Template01:
    Type: AWS::EC2::LaunchTemplate
    DependsOn: 
    - LB01
    - Ec2Instance
    Properties:
      LaunchTemplateData:
        DisableApiTermination: !Ref DeletionProtection
        InstanceType: !Ref InstanceType
        KeyName: !Ref KeyName
        ImageId: !Ref LatestAmiId
        SecurityGroupIds:
        - !GetAtt EC2SecurityGroupWEB.GroupId
        UserData:
          Fn::Base64:
            !Sub |
              #!/bin/bash
              yum update -y
              amazon-linux-extras install -y php7.2
              yum install -y httpd
              systemctl start httpd
              systemctl enable httpd
              usermod -a -G apache ec2-user
              chown -R ec2-user:apache /var/www
              chmod 2775 /var/www && find /var/www -type d -exec chmod 2775 {} \;
              find /var/www -type f -exec chmod 0664 {} \;
              yum install -y amazon-efs-utils
              echo "${FileSystem}:/ /var/www/html efs _netdev 0 0" | sudo tee -a /etc/fstab
              mount -a
      LaunchTemplateName: !Sub ${AWS::StackName}-Template01
  Ec2Instance: 
    Type: AWS::EC2::Instance
    DependsOn: 
    - LB01
    - FileSystem
    - Database
    Properties: 
      ImageId: !Ref LatestAmiId
      KeyName: !Ref KeyName
      InstanceType: !Ref InstanceType
      SecurityGroupIds:
        - !GetAtt EC2SecurityGroupWEB.GroupId
      SubnetId: !Ref PublicSubnetA
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash
            yum update -y
            amazon-linux-extras install -y php7.2
            yum install -y httpd
            systemctl start httpd
            systemctl enable httpd
            usermod -a -G apache ec2-user
            chown -R ec2-user:apache /var/www
            chmod 2775 /var/www && find /var/www -type d -exec chmod 2775 {} \;
            find /var/www -type f -exec sudo chmod 0664 {} \;
            wget https://wordpress.org/latest.tar.gz
            tar -xzf latest.tar.gz
            cp /wordpress/wp-config-sample.php /wordpress/wp-config.php
            sed -i -e "s/database_name_here/andersdb/g" /wordpress/wp-config.php
            sed -i -e "s/username_here/anderswp/g" /wordpress/wp-config.php
            sed -i -e "s/password_here/Grisfis123/g" /wordpress/wp-config.php
            sed -i -e "s/localhost/${Database.Endpoint.Address}/g" /wordpress/wp-config.php
            curl https://api.wordpress.org/secret-key/1.1/salt/ > salt.txt
            sed -i -e '49r salt.txt' -e '49,56d' /wordpress/wp-config.php
            cp -r wordpress/* /var/www/html/
            sed -i "151s/.*/AllowOverride All/" /etc/httpd/conf/httpd.conf
            curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
            chmod +x wp-cli.phar
            mv wp-cli.phar /usr/local/bin/wp
            cd /var/www/html/
            /usr/local/bin/wp core install --title=AndersBlog --admin_user=aadmin --admin_password=Grisfis123 --admin_email=a@a.com --url=${LB01.DNSName}
            yum install -y amazon-efs-utils
            mkdir -p /test/efs 
            mv /var/www/html/* /test/efs
            echo "${FileSystem}:/ /var/www/html efs _netdev 0 0" | sudo tee -a /etc/fstab
            mount -a
            mv /test/efs/* /var/www/html
      Tags:
        - Key: Name          
          Value: !Sub ${AWS::StackName}-ConfigurationServer            
  FileSystem:
    Type: AWS::EFS::FileSystem
    Properties:
      PerformanceMode: generalPurpose
      FileSystemTags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-EFSvolume
  MountTarget1:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId:
        Ref: FileSystem
      SubnetId:
        Ref: PublicSubnetA
      SecurityGroups:
      - Ref: MountTargetSecurityGroup
  MountTarget2:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId:
        Ref: FileSystem
      SubnetId:
        Ref: PublicSubnetB
      SecurityGroups:
      - Ref: MountTargetSecurityGroup    
  MountTarget3:
    Type: AWS::EFS::MountTarget
    Properties:
      FileSystemId:
        Ref: FileSystem
      SubnetId:
        Ref: PublicSubnetC
      SecurityGroups:
      - Ref: MountTargetSecurityGroup
  Database:
    Type: 'AWS::RDS::DBInstance'
    Properties:
      AllocatedStorage: '5'
      DBInstanceClass: 'db.t2.micro'
      DBName: 'andersdb'
      Engine: 'MariaDB'
      EngineVersion: 10.2.21
      MasterUsername: 'anderswp'
      MasterUserPassword: 'Grisfis123'
      StorageType: 'gp2'
      MultiAZ: 'true'
      DBSubnetGroupName: !Ref DBSubnetGroup
      VPCSecurityGroups:
        - !GetAtt DatabaseSecurityGroup.GroupId
  DBSubnetGroup:
    Type: 'AWS::RDS::DBSubnetGroup'
    Properties:
      DBSubnetGroupDescription: 'DB subnet group'
      SubnetIds:
        - !Ref PublicSubnetA
        - !Ref PublicSubnetB
        - !Ref PublicSubnetC