$password = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
$username = "administrator" 
$credential = New-Object System.Management.Automation.PSCredential($username,$password)

$VMNames = Get-VM | Where-Object Name -NotLike "MOV-DC01", "MOV-DC02"

foreach($VMName in $VMNames){

Invoke-Command -VMName $VMName.VMName -Credential $credential -ScriptBlock {
    $DomainPassword = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
    $DomainUsername = "administrator" 
    $DomainCredential = New-Object System.Management.Automation.PSCredential($DomainUsername,$DomainPassword)
    Add-Computer -DomainName "ad.mov.se" -OUPath "OU=Computers,OU=MOV,DC=ad,DC=mov,DC=se" -Credential $DomainCredential
    Restart-Computer -Force
    }
}

#################################

$password = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
$username = "administrator" 
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
$VMName = MOV-FS01
#$VMName = MOV-CLIENT01

Invoke-Command -VMName $VMName -Credential $credential -ScriptBlock {
    $DomainPassword = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
    $DomainUsername = "administrator" 
    $DomainCredential = New-Object System.Management.Automation.PSCredential($DomainUsername,$DomainPassword)
    Add-Computer -DomainName "ad.mov.se" -OUPath "OU=Computers,OU=MOV,DC=ad,DC=mov,DC=se" -Credential $DomainCredential
    Restart-Computer -Force
    }
    
    
##############################

$DomainPassword = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
$DomainUsername = "MOV\administrator" 
$DomainCredential = New-Object System.Management.Automation.PSCredential($DomainUsername,$DomainPassword)

#$VMName = "MOV-FS01"
$VMName = "MOV-CLIENT01"

Invoke-Command -VMName $VMName -Credential $DomainCredential -ScriptBlock {
Enable-NetFireWallRule FPS-ICMP4-ERQ-In
}


#####################################

$password = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
$username = "administrator" 
$credential = New-Object System.Management.Automation.PSCredential($username,$password)


Invoke-Command -VMName MOV-DC02 -Credential $credential -ScriptBlock {
$DomainPassword = ConvertTo-SecureString -String "P@ssw0rd" -asPlainText -Force
$DomainUsername = "mov\administrator" 
$DomainCredential = New-Object System.Management.Automation.PSCredential($DomainUsername,$DomainPassword)
Get-WindowsFeature AD-Domain-Services | Install-WindowsFeature -IncludeManagementTools

Import-Module ADDSDeployment
Install-ADDSDomainController `
-NoGlobalCatalog:$false `
-CreateDnsDelegation:$false `
-Credential $DomainCredential  `
-CriticalReplicationOnly:$false `
-DatabasePath "C:\Windows\NTDS" `
-DomainName "ad.mov.se" `
-InstallDns:$true `
-LogPath "C:\Windows\NTDS" `
-NoRebootOnCompletion:$false `
-ReplicationSourceDC "MOV-DC01.ad.mov.se" `
-SiteName "Default-First-Site-Name" `
-SysvolPath "C:\Windows\SYSVOL" `
-SafeModeAdministratorPassword $DomainPassword `
-Force:$true
}